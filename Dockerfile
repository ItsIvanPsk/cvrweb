﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 55555
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["./ItsyDev.ApplicationServices.WebPage/ItsyDev.ApplicationServices.WebPage.csproj", "ItsyDev.ApplicationServices.WebPage/"]
COPY ["./ItsyDev.ApplicationServices.Services/ItsyDev.ApplicationServices.Services.csproj", "ItsyDev.ApplicationServices.Services/"]
COPY ["./ItsyDev.ApplicationServices.Models/ItsyDev.ApplicationServices.Models.csproj", "ItsyDev.ApplicationServices.Models/"]
COPY ["./ItsyDev.CrossCutting.Models/ItsyDev.CrossCutting.Models.csproj", "ItsyDev.CrossCutting.Models/"]
COPY ["./ItsyDev.DomainServices.Domain/ItsyDev.DomainServices.Domain.csproj", "ItsyDev.DomainServices.Domain/"]
COPY ["./ItsyDev.DomainServices.Models/ItsyDev.DomainServices.Models.csproj", "ItsyDev.DomainServices.Models/"]
COPY ["./ItsyDev.DomainServices.RepositoryContracts/ItsyDev.DomainServices.RepositoryContracts.csproj", "ItsyDev.DomainServices.RepositoryContracts/"]
COPY ["./ItsyDev.InfraestructureServices.Models/ItsyDev.InfraestructureServices.Models.csproj", "ItsyDev.InfraestructureServices.Models/"]
COPY ["./ItsyDev.CrossCutting.Utils/ItsyDev.CrossCutting.Utils.csproj", "ItsyDev.CrossCutting.Utils/"]
COPY ["./ItsyDev.InfraestructureServices.Repository/ItsyDev.InfraestructureServices.Repository.csproj", "ItsyDev.InfraestructureServices.Repository/"]

RUN dotnet restore "ItsyDev.ApplicationServices.WebPage/ItsyDev.ApplicationServices.WebPage.csproj"
COPY . .
WORKDIR "/src/ItsyDev.ApplicationServices.WebPage"
RUN dotnet build "ItsyDev.ApplicationServices.WebPage.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ItsyDev.ApplicationServices.WebPage.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ItsyDev.ApplicationServices.WebPage.dll"]
