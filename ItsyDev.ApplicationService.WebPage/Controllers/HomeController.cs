﻿using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.ApplicationServices.WebPage.Controllers;

public class HomeController : Controller
{
    public IActionResult Index()
    {
        return View();
    }
}