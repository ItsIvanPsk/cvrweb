﻿using System.Threading.Tasks;
using ItsyDev.ApplicationServices.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.ApplicationServices.WebPage.Controllers;

public class ImagesController : Controller
{
    private readonly IImageService _imageService;

    public ImagesController(IImageService imageService)
    {
        _imageService = imageService;
    }

    public async Task<IActionResult> Index()
    {
        var result = await _imageService.GetImages();
        return View(result);
    }
}