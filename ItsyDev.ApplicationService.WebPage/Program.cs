using AutoMapper;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.ApplicationServices.Services.Implementations;
using ItsyDev.CrossCutting.Utils;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Domain.Implementations;
using ItsyDev.DomainServices.RepositoryContracts;
using ItsyDev.InfraestructureServices.Repository.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();


// DI - Experiences
builder.Services.AddScoped<IExperienceService, ExperienceService>();
builder.Services.AddScoped<IExperienceDomain, ExperienceDomain>();
builder.Services.AddScoped<IExperienceRepository, ExperienceRepository>();
builder.Services.AddScoped<IExperienceService, ExperienceService>();

// DI - Images
builder.Services.AddScoped<IImageService, ImageService>();
builder.Services.AddScoped<IImageDomain, ImageDomain>();
builder.Services.AddScoped<IImageRepository, ImageRepository>();
builder.Services.AddScoped<IImageService, ImageService>();

builder.Services.AddScoped<IJsonFileManager, JsonFileManager>();

// Mapper Configurator
var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
var mapper = mappingConfig.CreateMapper();
builder.Services.AddSingleton(mapper);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    "default",
    "{controller=Home}/{action=Index}/{id?}");

app.Run();