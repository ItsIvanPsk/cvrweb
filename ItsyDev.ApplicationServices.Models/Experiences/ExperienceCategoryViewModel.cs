﻿using System;
using ItsyDev.CrossCutting.Models.Base_Models;

namespace ItsyDev.ApplicationServices.Models.Experiences;

public class ExperienceCategoryViewModel : BaseModel
{
    public string Name { get; set; } = string.Empty;

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;

        var otherObject = (ExperienceCategoryViewModel)obj;

        return string.Equals(Name, otherObject.Name, StringComparison.OrdinalIgnoreCase);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hash = 17;
            hash = hash * 23 + (Name != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Name) : 0);
            return hash;
        }
    }
}