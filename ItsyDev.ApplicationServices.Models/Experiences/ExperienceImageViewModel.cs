﻿namespace ItsyDev.ApplicationServices.Models.Experiences;

public class ExperienceImageViewModel
{
    public string Src { get; set; } = string.Empty;
}