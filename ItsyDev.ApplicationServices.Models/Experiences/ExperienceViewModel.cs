﻿using System.Collections.Generic;
using ItsyDev.ApplicationServices.Models.Materials;
using ItsyDev.CrossCutting.Models.Base_Models;

namespace ItsyDev.ApplicationServices.Models.Experiences;

public class ExperienceViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

    public float Rating { get; set; }

    public List<ExperienceCategoryViewModel>? Categories { get; set; }

    public List<ExperienceImageViewModel>? Images { get; set; }

    public List<MaterialViewModel>? Material { get; set; }
}