﻿using System;

namespace ItsyDev.ApplicationServices.Models.Images;

public class ImageViewModel
{
    public string Src { get; set; } = string.Empty;
    public string MonthName { get; set; } = string.Empty;
    public DateTime Date { get; set; }
    public string Description { get; set; } = string.Empty;
}