﻿using System;
using ItsyDev.CrossCutting.Models.Base_Models;

namespace ItsyDev.ApplicationServices.Models.Materials;

public class MaterialViewModel : BaseModel
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;

        var otherMaterial = (MaterialViewModel)obj;

        return string.Equals(Name, otherMaterial.Name, StringComparison.OrdinalIgnoreCase)
               && string.Equals(Description, otherMaterial.Description, StringComparison.OrdinalIgnoreCase);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hash = 17;
            hash = hash * 23 + (Name != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Name) : 0);
            hash = hash * 23 + (Description != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(Description) : 0);
            return hash;
        }
    }
}