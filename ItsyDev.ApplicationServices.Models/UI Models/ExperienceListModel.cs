﻿using System.Collections.Generic;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Models.Materials;

namespace ItsyDev.ApplicationServices.Models.UI_Models;

public class ExperienceListModel
{
    public List<ExperienceViewModel>? Experiences { get; set; }
    public List<ExperienceCategoryViewModel>? Categories { get; set; }
    public List<MaterialViewModel>? Headsets { get; set; }

    public string? NameSelected { get; set; } = string.Empty;

    public bool? CategorySelected { get; set; }

    public bool? MaterialSelected { get; set; }
}