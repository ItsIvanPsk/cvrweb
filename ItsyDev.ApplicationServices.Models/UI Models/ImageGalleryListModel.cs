﻿using System.Collections.Generic;
using ItsyDev.ApplicationServices.Models.Images;

namespace ItsyDev.ApplicationServices.Models.UI_Models;

public class ImageGalleryListModel
{
    public List<ImageViewModel> Images { get; set; }
}