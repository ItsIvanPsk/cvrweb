﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Models.Materials;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface IExperienceService
{
    Task<List<ExperienceViewModel>> GetExperiences();
    Task<ExperienceViewModel> GetExperienceById(int experienceId);
    List<MaterialViewModel>? GetAvailableHeadsetsByExperiences(List<ExperienceViewModel> experiences);
    List<ExperienceCategoryViewModel>? GetAvailableCategoriesByExperiences(List<ExperienceViewModel> experiences);
}