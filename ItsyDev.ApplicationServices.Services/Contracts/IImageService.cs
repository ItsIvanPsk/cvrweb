﻿using System.Threading.Tasks;
using ItsyDev.ApplicationServices.Models.Images;
using ItsyDev.ApplicationServices.Models.UI_Models;

namespace ItsyDev.ApplicationServices.Services.Contracts;

public interface IImageService
{
    Task<ImageGalleryListModel> GetImages();
}