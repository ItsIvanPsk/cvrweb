﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Models.Materials;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class ExperienceService : IExperienceService
{
    private readonly IExperienceDomain _experienceDomain;
    private readonly IMapper _mapper;

    public ExperienceService(IExperienceDomain experienceDomain, IMapper mapper)
    {
        _experienceDomain = experienceDomain;
        _mapper = mapper;
    }

    public async Task<List<ExperienceViewModel>> GetExperiences()
    {
        return _mapper.Map<List<ExperienceViewModel>>(await _experienceDomain.GetExperiences());
    }

    public async Task<ExperienceViewModel> GetExperienceById(int experienceId)
    {
        return _mapper.Map<ExperienceViewModel>(await _experienceDomain.GetExperienceById(experienceId));
    }

    public List<MaterialViewModel>? GetAvailableHeadsetsByExperiences(List<ExperienceViewModel> experiences)
    {
        var materials = experiences.SelectMany(e => e.Material);
        var availableMaterials = materials.Distinct().ToList();
        return availableMaterials;
    }

    public List<ExperienceCategoryViewModel>? GetAvailableCategoriesByExperiences(List<ExperienceViewModel> experiences)
    {
        var categories = experiences.SelectMany(e => e.Categories);
        var availableCategories = categories.Distinct().ToList();
        return availableCategories;
    }

    public async Task<List<ExperienceViewModel>> FilterExperiences(string name, int deviceId, int categoryId)
    {
        var experiences = await GetExperiences();

        var filteredExperiences = experiences
            .Where(e =>
                (string.IsNullOrEmpty(name) || e.Name.StartsWith(name)) &&
                (deviceId == 0 || e.Material.Any(material => material.Id == deviceId)) &&
                (categoryId == 0 || e.Categories.Any(category => category.Id == categoryId))
            )
            .ToList();

        return filteredExperiences;
    }
}