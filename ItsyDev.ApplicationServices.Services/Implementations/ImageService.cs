﻿using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ItsyDev.ApplicationServices.Models.Images;
using ItsyDev.ApplicationServices.Models.UI_Models;
using ItsyDev.ApplicationServices.Services.Contracts;
using ItsyDev.DomainServices.Domain.Contracts;

namespace ItsyDev.ApplicationServices.Services.Implementations;

public class ImageService : IImageService
{
    private readonly IImageDomain _domain;

    public ImageService(IImageDomain domain)
    {
        _domain = domain;
    }

    public async Task<ImageGalleryListModel> GetImages()
    {
        var result = await _domain.GetImages();
        var images = new ImageGalleryListModel()
        {
            Images = result.Select(r => new ImageViewModel
            {
                Src = r.Src,
                MonthName = r.MonthName,
                Date = r.Date,
                Description = r.Description
            }).ToList()
        };
        return images;
    }
}