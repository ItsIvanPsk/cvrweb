﻿using System;
using System.Threading.Tasks;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.ApplicationServices.WebPage.Controllers;

public class ExperienceDetailController : Controller
{
    private readonly IExperienceService _experienceService;

    public ExperienceDetailController(IExperienceService experienceService)
    {
        _experienceService = experienceService;
    }

    public async Task<IActionResult> Index(int? experienceId)
    {
        try
        {
            var id = -1;
            if (experienceId.HasValue) id = experienceId.Value;
            var experience = await GetExperienceById(id);
            View(experience);
        }
        catch (ArgumentNullException ex)
        {
        }

        return View();
    }

    public async Task<ExperienceViewModel> GetExperienceById(int experienceId)
    {
        return await _experienceService.GetExperienceById(experienceId);
    }
}