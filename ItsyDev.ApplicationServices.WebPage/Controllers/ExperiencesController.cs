﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Models.Materials;
using ItsyDev.ApplicationServices.Models.UI_Models;
using ItsyDev.ApplicationServices.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ItsyDev.ApplicationServices.WebPage.Controllers;

public class ExperiencesController : Controller
{
    private readonly IExperienceService _experienceService;
    private readonly List<ExperienceCategoryViewModel>? _experienceCategories;
    private readonly List<MaterialViewModel>? _experienceMaterials;

    public ExperiencesController(IExperienceService experienceService)
    {
        _experienceService = experienceService;
        _experienceCategories = new List<ExperienceCategoryViewModel>();
        _experienceMaterials = new List<MaterialViewModel>();
    }

    public async Task<IActionResult> Index()
    {
        var experiences = await _experienceService.GetExperiences();
        return View(new ExperienceListModel
        {
            Experiences = experiences,
        });
    }

    public IActionResult FilterExperiences()
    {
        return RedirectToAction("Index");
    }

    public async Task<List<ExperienceViewModel>> GetExperiences()
    {
        var result = await _experienceService.GetExperiences();
        return result.Count >= 1 ? result : new List<ExperienceViewModel>();
    }

    public List<MaterialViewModel>? GetExperienceHeadsets(List<ExperienceViewModel> experiences)
    {
        if (_experienceMaterials is not { Count: 0 }) return _experienceMaterials;
        if (experiences.Count == 0)
            return new List<MaterialViewModel>();
        var headsets = _experienceService.GetAvailableHeadsetsByExperiences(experiences);
        return headsets;
    }

    public List<ExperienceCategoryViewModel>? GetExperienceCategories(List<ExperienceViewModel> experiences)
    {
        if (_experienceCategories is not { Count: 0 }) return _experienceCategories;
        if (experiences.Count == 0)
            return new List<ExperienceCategoryViewModel>();
        var categories = _experienceService.GetAvailableCategoriesByExperiences(experiences);
        return categories;
    }
}