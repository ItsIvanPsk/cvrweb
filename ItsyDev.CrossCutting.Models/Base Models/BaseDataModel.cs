﻿using System;

namespace ItsyDev.CrossCutting.Models.Base_Models;

public abstract class BaseDataModel
{
    public float Id { get; set; }
    public Guid Guid { get; set; }
}