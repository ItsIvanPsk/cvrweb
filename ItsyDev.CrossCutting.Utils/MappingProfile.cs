﻿using AutoMapper;
using ItsyDev.ApplicationServices.Models.Experiences;
using ItsyDev.ApplicationServices.Models.Images;
using ItsyDev.ApplicationServices.Models.Materials;
using ItsyDev.DomainServices.Models.Experiences;
using ItsyDev.DomainServices.Models.Images;
using ItsyDev.DomainServices.Models.Materials;
using ItsyDev.InfraestructureServices.Models.Experiences;
using ItsyDev.InfraestructureServices.Models.Images;
using ItsyDev.InfraestructureServices.Models.Materials;

namespace ItsyDev.CrossCutting.Utils;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<ExperienceViewModel, ExperienceBe>().ReverseMap();
        CreateMap<ExperienceBe, ExperienceDto>().ReverseMap();

        CreateMap<ExperienceCategoryViewModel, ExperienceCategoryBe>().ReverseMap();
        CreateMap<ExperienceCategoryBe, ExperienceCategoryDto>().ReverseMap();

        CreateMap<ExperienceImageViewModel, ExperienceImageBe>().ReverseMap();
        CreateMap<ExperienceImageBe, ExperienceImageDto>().ReverseMap();

        CreateMap<MaterialViewModel, MaterialBe>().ReverseMap();
        CreateMap<MaterialBe, MaterialDto>().ReverseMap();

        CreateMap<ImageDto, ImageBe>().ReverseMap();
    }
}