﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.DomainServices.Models.Experiences;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface IExperienceDomain
{
    Task<List<ExperienceBe>> GetExperiences();
    Task<ExperienceBe> GetExperienceById(int experienceId);
}