﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.DomainServices.Models.Images;

namespace ItsyDev.DomainServices.Domain.Contracts;

public interface IImageDomain
{
    Task<List<ImageBe>> GetImages();
}