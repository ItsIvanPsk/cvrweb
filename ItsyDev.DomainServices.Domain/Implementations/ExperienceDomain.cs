﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Experiences;
using ItsyDev.DomainServices.RepositoryContracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class ExperienceDomain : IExperienceDomain
{
    private readonly IExperienceRepository _experienceRepository;
    private readonly IMapper _mapper;

    public ExperienceDomain(IExperienceRepository experienceRepository, IMapper mapper)
    {
        _experienceRepository = experienceRepository;
        _mapper = mapper;
    }

    public async Task<List<ExperienceBe>> GetExperiences()
    {
        return _mapper.Map<List<ExperienceBe>>(await _experienceRepository.GetExperiences());
    }

    public async Task<ExperienceBe> GetExperienceById(int experienceId)
    {
        return _mapper.Map<ExperienceBe>(await _experienceRepository.GetExperienceById(experienceId));
    }
}