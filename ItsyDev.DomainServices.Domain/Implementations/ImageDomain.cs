﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ItsyDev.DomainServices.Domain.Contracts;
using ItsyDev.DomainServices.Models.Images;
using ItsyDev.DomainServices.RepositoryContracts;

namespace ItsyDev.DomainServices.Domain.Implementations;

public class ImageDomain : IImageDomain
{
    private readonly IImageRepository _repository;
    private readonly IMapper _mapper;

    public ImageDomain(IImageRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<ImageBe>> GetImages()
    {
        var result = await _repository.GetImages();
        return _mapper.Map<List<ImageBe>>(result);
    }
}