﻿using System.Collections.Generic;
using ItsyDev.CrossCutting.Models.Base_Models;
using ItsyDev.DomainServices.Models.Materials;

namespace ItsyDev.DomainServices.Models.Experiences;

public class ExperienceBe : BaseModel
{
    public float Id { get; set; }

    public string Name { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

    public float Rating { get; set; } = 0;

    public List<ExperienceCategoryBe>? Categories { get; set; }

    public List<ExperienceImageBe>? Images { get; set; }

    public List<MaterialBe>? Material { get; set; }
}