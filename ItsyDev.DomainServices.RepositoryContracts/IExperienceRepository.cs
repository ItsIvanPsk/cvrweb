﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.InfraestructureServices.Models.Experiences;

namespace ItsyDev.DomainServices.RepositoryContracts;

public interface IExperienceRepository
{
    Task<List<ExperienceDto>> GetExperiences();
    Task<ExperienceDto> GetExperienceById(int experienceId);
}