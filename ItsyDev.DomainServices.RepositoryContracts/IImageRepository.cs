﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ItsyDev.InfraestructureServices.Models.Images;

namespace ItsyDev.DomainServices.RepositoryContracts;

public interface IImageRepository
{
    Task<List<ImageDto>> GetImages();
}