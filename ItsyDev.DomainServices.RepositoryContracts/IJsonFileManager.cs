﻿using System.Threading.Tasks;

namespace ItsyDev.DomainServices.RepositoryContracts;

public interface IJsonFileManager
{
    bool JsonFileExists(string path);
    Task<string> ReadJsonFile(string path);
    T? DeserializeObject<T>(string json);
}