﻿using ItsyDev.CrossCutting.Models.Base_Models;
using Newtonsoft.Json;

namespace ItsyDev.InfraestructureServices.Models.Experiences;

public class ExperienceCategoryDto : BaseModel
{
    [JsonProperty("id")] public int Id { get; set; }
    [JsonProperty("name")] public string Name { get; set; } = string.Empty;
}