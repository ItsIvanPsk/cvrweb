﻿using System.Collections.Generic;
using ItsyDev.InfraestructureServices.Models.Materials;
using Newtonsoft.Json;

namespace ItsyDev.InfraestructureServices.Models.Experiences;

public class ExperienceDto
{
    [JsonProperty("experience_id")] public int Id { get; set; }

    [JsonProperty("name")] public string Name { get; set; } = string.Empty;

    [JsonProperty("description")] public string Description { get; set; } = string.Empty;

    [JsonProperty("rating")] public float Rating { get; set; } = 0;

    [JsonProperty("categories")] public List<ExperienceCategoryDto> Categories { get; set; } = new();

    [JsonProperty("images")] public List<ExperienceImageDto> Images { get; set; } = new();

    [JsonProperty("headsets")] public List<MaterialDto> Material { get; set; } = new();
}