﻿using Newtonsoft.Json;

namespace ItsyDev.InfraestructureServices.Models.Experiences;

public class ExperienceImageDto
{
    [JsonProperty("src")] public string Src { get; set; } = string.Empty;
}