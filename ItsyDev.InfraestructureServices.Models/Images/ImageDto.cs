﻿using System;

namespace ItsyDev.InfraestructureServices.Models.Images;

public class ImageDto
{
    public int Id { get; set; }
    public string Src { get; set; } = string.Empty;
    public DateTime Date { get; set; }
    public string MonthName { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
}