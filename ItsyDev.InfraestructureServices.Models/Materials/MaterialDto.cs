﻿using Newtonsoft.Json;

namespace ItsyDev.InfraestructureServices.Models.Materials;

public class MaterialDto
{
    [JsonProperty("id")] public int Id { get; set; }

    [JsonProperty("name")] public string Name { get; set; } = string.Empty;

    [JsonProperty("description")] public string Description { get; set; } = string.Empty;
}