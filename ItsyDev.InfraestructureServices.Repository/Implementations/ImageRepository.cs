﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ItsyDev.DomainServices.RepositoryContracts;
using ItsyDev.InfraestructureServices.Models.Images;

namespace ItsyDev.InfraestructureServices.Repository.Implementations;

public class ImageRepository : IImageRepository
{
    private readonly IJsonFileManager _jsonFileManager;

    public ImageRepository(IJsonFileManager jsonFileManager)
    {
        _jsonFileManager = jsonFileManager;
    }

    public async Task<List<ImageDto>> GetImages()
    {
        try
        {
            var hardcodedJson =
                "[\n    {\n        \"Id\": 1,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Headsets/Vive/htc_vive_logo.png\",\n        \"Date\": \"01/30/2024 15:45:30\",\n        \"MonhtName\": \"February\",\n        \"Description\": \"Evento 1\"\n    },\n    {\n        \"Id\": 2,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"12/30/2023 15:45:30\",\n        \"MonhtName\": \"December\",\n        \"Description\": \"Evento 2\"\n    },\n    {\n        \"Id\": 3,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"10/30/2023 15:45:30\",\n        \"MonhtName\": \"November\",\n        \"Description\": \"Evento 3\"\n    },\n    {\n        \"Id\": 4,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"9/30/2023 15:45:30\",\n        \"MonhtName\": \"October\",\n        \"Description\": \"Evento 4\"\n    },\n    {\n        \"Id\": 5,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2023 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 6,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2021 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 7,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2021 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 8,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2021 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 9,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2020 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 10,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2019 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 11,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2020 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    },\n    {\n        \"Id\": 12,\n        \"Src\": \"https://raw.githubusercontent.com/ItsIvanPsk/ClubVRAssets/master/Experiences/9-Contractors/contractors_1.jpg\",\n        \"Date\": \"8/30/2021 15:45:30\",\n        \"MonhtName\": \"August\",\n        \"Description\": \"Evento 5\"\n    }\n    \n]";
            var exp = _jsonFileManager.DeserializeObject<List<ImageDto>>(hardcodedJson);
            return exp ?? throw new InvalidOperationException();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }


        if (!_jsonFileManager.JsonFileExists(AppDomain.CurrentDomain.BaseDirectory + "./Experiences.json"))
            throw new FileNotFoundException(nameof(ExperienceRepository));

        var experienceJson =
            await _jsonFileManager.ReadJsonFile(AppDomain.CurrentDomain.BaseDirectory + "./Experiences.json");
        var experiences = new List<ImageDto>();
        try
        {
            experiences = _jsonFileManager.DeserializeObject<List<ImageDto>>(experienceJson);
        }
        catch (FileLoadException ex)
        {
        }

        return experiences ?? throw new InvalidOperationException();
    }

    public async Task<ImageDto> GetImageById(int imageId)
    {
        var images = await GetImages();
        var image = images.FirstOrDefault(e => e.Id == imageId);
        return image ?? throw new InvalidOperationException();
    }
}