﻿using System;
using System.IO;
using System.Threading.Tasks;
using ItsyDev.DomainServices.RepositoryContracts;
using Newtonsoft.Json;

namespace ItsyDev.InfraestructureServices.Repository.Implementations;

public class JsonFileManager : IJsonFileManager
{
    public bool JsonFileExists(string path)
    {
        return File.Exists(path);
    }

    public async Task<string> ReadJsonFile(string path)
    {
        var content = await File.ReadAllTextAsync(path);
        return content;
    }

    public T? DeserializeObject<T>(string json)
    {
        try
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        catch (JsonException ex)
        {
            throw new Exception("Error de deserialización JSON: " + ex.Message);
        }
    }
}